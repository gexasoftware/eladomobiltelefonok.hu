var app = {
    // Application Constructor
    offset: 0,
    limit: 20,
    loaded: false,
    advShow: false,

    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        //console.log('Received Device Ready Event');
        //console.log('calling setup push');
        localStorage.removeItem('registrationId');
        app.init();
    },
    setupPush: function() {
        var _t = this;
        var push = PushNotification.init({
            "android": {
                "senderID": "126392252512"
            },
            "browser": {},
            "ios": {
                "sound": true,
                "vibration": true,
                "badge": true
            },
            "windows": {}
        });
        
        push.on('registration', function(data) {
            localStorage.setItem('registrationId', data.registrationId);
            _t.ajax_request('registerDevice', { newId: data.registrationId }, function(JSONResponse) {
                if (!JSONResponse || JSONResponse.success != 1) {
                    infoMessage(JSONResponse.message, 'Üzenet');
                    localStorage.removeItem('loggedIn');
                    window.location.reload(true);
                    return;
                }
            });
        });

        push.on('error', function(e) {
            console.log("push error = " + e.message);
        });

        push.on('notification', function(data) {
            //console.log('notification event');
            navigator.notification.alert(
                data.message,         // message
                null,                 // callback
                data.title,           // title
                'Ok'                  // buttonName
            );
       });
    },
    init: function() {
        var loggedIn = localStorage.getItem('loggedIn');
        if (!loggedIn || loggedIn < 1) {
            $('.loggedin').hide();
            $('#loginForm').on('submit', function() {
                app.ajax_request('login', {
                    login_email: $('#login_email').val(),
                    login_pass: md5($('#login_pass').val())
                }, function(JSONResponse) {
                    if (!JSONResponse || JSONResponse.success != 1) {
                        errorMessage('Hibás belépési adatok!');
                        return false;
                    }

                    if (JSONResponse.id > 0) {
                        localStorage.setItem('loggedIn', JSONResponse.id);
                        $('#loginForm').find('input').val('');
                        app.setLoggedIn();
                        return true;
                    }
                    return false;
                });
            });
        } else {
            app.setLoggedIn();
            app.search();
            app.ajax_request('list_category', { a: Math.random() }, function(JSONResponse) {
                if (!JSONResponse.data || !JSONResponse.data.length) {
                    return false;
                }

                $('#category').find('option').not(':first').remove();
                $.each(JSONResponse.data, function(i, o) {
                    $('#category').append('<option value="'+o.id+'">'+o.name+'</option>');
                });
            });
        }        

        var parentElement = document.getElementById('registration');
        var listeningElement = $('.waiting');
        var receivedElement = $('.received');

        listeningElement.hide();
        receivedElement.show();
    },
    logout: function() {
        if (confirm('Biztos benne, hogy kijelentkezik az adminisztrációs fiókjából?')) {
            app.ajax_request('logout', { a: Math.random() }, function() {});
            localStorage.removeItem('loggedIn');
            window.location.reload(true);
        }
    },
    setLoggedIn: function() {
        $('.loggedout, #loginForm').remove();
        $('.loggedin').show();
        app.setupPush();
        app.search();
        app.getSettings();
    },
    search: function() {
        
        $('#search').rmodal('hide');

        this.ajax_request('search', {
            offset: app.offset,
            limit: app.limit,
            search: {
                text: $('#text').val(),
                category: $('#category').val(),
                status: $('#active').hasClass('active') ? 2 : ($('#inactive').hasClass('active') ? 1 : 0)
            }
        }, function(JSONResponse) {
            if ((!JSONResponse.list || !JSONResponse.list.length) && app.offset == 0) {
                infoMessage('Nincs találat!', '...');
                $('#result > #list').html('');
                return;
            }

            if (app.offset == 0) {
                $('#result > #list').html('');
            }

            //console.log(JSONResponse);
            $.each(JSONResponse.list, function(i, ad) {
                var tpl = app.getAdTpl(ad);
                $('#result > #list').append(tpl);
            });

            app.loaded = true;
        });
    },
    resetSearch: function() {
        $('#search').find('input, select').val('');
        $('#search').find('.toggle').removeClass('active');
        app.search();
        $('#search').rmodal('hide');
    },
    getAdTpl: function(data) {
        var tpl = '', active = false, date, now, activated;

        date = new Date(data.activate_end);
        now = new Date();
        activated = new Date(data.activated);
        var modified = new Date(data.time_modify);
        active = data.active != 0 && date>now ? true : false;
        var other = '';

        if (data.active!=1) {
            other += ' <small style="color: red">Inaktív | </small>';
        } 

        if (active!=1 && activated < modified) {
            other += ' <small style="color: red">Módosítva | </small>';
        } 

        if (date < now) {
            other += ' <small style="color: red">Lejárt | </small>';
        }

        var disallowed = '';
        if (data.not_allowed && data.not_allowed.length >= 10 && data.active != 1) {
            disallowed = '<small style="color: red">Elutasítva | </small>';
        }

        tpl += '<li class="table-view-cell one-adv">';
        tpl += '    <a onclick="app.editAdvert('+data.id+');" class="btn btn-positive pull-left edit"><span class="icon icon-edit"></span></a>';
        tpl += '    <a onclick="app.showAdvert('+data.id+');">'+data.title+other+disallowed+'</a>';
        tpl += '    <div class="toggle'+(active ? ' active' : '')+'" onclick="app.activateAdvert('+data.id+', this);"> \
                        <div class="toggle-handle"></div> \
                    </div>';
        tpl += '</li>';
        return tpl;
    },
    activateAdvert: function(id, obj) {
        var active = $(obj).hasClass('active') ? 1 : 0;
        app.ajax_request('activate_ad', {
            id: id,
            active: active
        }, function(JSONResponse) {
            if (JSONResponse.success != 1) {
                errorMessage('Hiba történt!');
                return;
            }
            infoMessage('Sikeresen '+(active==1?'aktiválta':'inaktiválta')+' a hirdetést!', 'Sikeres mentés!');
            app.search();
        });
    },
    advertTpl: function(id, data) {
         var tpl = '', active = false, date, now, activated;

        date = new Date(data.activate_end);
        now = new Date();
        activated = new Date(data.activated);
        var modified = new Date(data.time_modify);
        active = data.active != 0 && date>now ? true : false;
        var other = '';

        if (data.active!=1) {
            other += ' <small style="color: red">Inaktív | </small>';
        } 

        if (active!=1 && activated < modified) {
            other += ' <small style="color: red">Módosítva | </small>';
        } 

        if (date < now) {
            other += ' <small style="color: red">Lejárt | </small>';
        }

        var disallowed = '';
        if (data.not_allowed && data.not_allowed.length >= 10 && data.active != 1) {
            disallowed = '<h4 style="color: #c00; width: 100%; padding: 10px; margin: 10px 0; border: 2px dotted #DDD; box-sizing: border-box;">Elutasítva: '+data.not_allowed+'</h4>';
        }

        var category = ' <small>(kategória: <b>'+data.name+'</b>)</small>';
        //console.log(category);
        tpl += '<h3>'+data.title + category +'<br>'+other +'</h3>';
        tpl += disallowed;
        tpl += '<div class="toggle-with-text pull-left"> \
                    <div class="toggle'+(active ? ' active' : '')+'" onclick="app.activateAdvert('+data.id+', this);"> \
                        <div class="toggle-handle"></div> \
                    </div> \
                    <div class="toggle-label"> \
                       '+(active ? 'Inaktiválás' : 'Aktiválás')+' \
                    </div> \
                </div>';

        if (data.active != 1) {
            tpl += '<div class="options pull-right">';
            tpl += '    <a href="javascript:app.disallowAdvert('+id+');" class="btn btn-negative margin-left"><span class="icon icon-close"></span> Elutasítás</a>';
            tpl += '</div>';
        }
        tpl += '<div class="options pull-right">';
        tpl += '    <a href="javascript:app.editAdvert('+id+');" class="btn btn-positive"><span class="icon icon-edit"></span> Szerkesztés</a>';
        tpl += '</div>';


        tpl += '<div class="ad-row">';
        tpl += '    <h3>Hirdető</h3>';
        tpl += '    <p>'+data.lastname+' '+data.firstname+'<br>e-mail: '+data.email+',<br>telefon: '+data.phone+'</p>';
        tpl += '</div>';

        if (data.images.length > 5) {
            var images = JSON.parse(data.images);
            tpl += '<div class="full-width float-left clear">Összesen <strong>'+(images.length)+'</strong> db fotó</div>';
            tpl += '<div class="slider" id="mySlider"> \
                        <div class="slide-group">';
            $.each(images, function(i, img) {
                tpl += '<div class="slide">';
                tpl += '    <img src="https://eladomobiltelefonok.hu/'+img+'" style="max-width: 100%">';
                if (i == 0 && images.length > 1) {
                    tpl += '<span class="slide-text"> \
                                <span class="icon icon-left-nav"></span> \
                                Lapozás \
                            </span>';
                }
                tpl += '</div>';
            });
        }
        tpl += '    </div>';
        tpl += '</div>';

        tpl += '<div class="ad-row">';
        tpl += '    <h3>Leírás</h3>';
        tpl += '    <p>'+data.description+'</p>';
        tpl += '</div>';

        tpl += '<div class="ad-row">';
        tpl += '    <h3>Itt található</h3>';
        tpl += '    <p>'+data.zip+' '+data.city+'</p>';
        tpl += '</div>';

        tpl += '<div class="ad-row">';
        tpl += '    <h3>Egyéb információk</h3>';
        tpl += '    <p>Születési idő: '+data.birth+'</p>';
            $.each(data.props, function(ix, prop) {
                if (prop.prop_type != 'check') {
                    if (prop.value != '') {
                        tpl += '<p><b>'+(prop.prop_name+':</b> '+prop.value)+'</p>';
                    }
                } else {
                    if (prop.value == 1) {
                        tpl += '<p><b>'+(prop.prop_name)+'</b></p>';
                    }
                }
            });
        tpl += '</div>';

        return tpl;
    },
    showAdvert: function(id) {
        //console.log(id);
        app.ajax_request('show_advert', {id: id, a: Math.random() }, function(JSONResponse) {
            if ($('#advert_'+id).length == 0) {
                $('.app').append('<div id="advert_'+id+'" style="padding: 30px; box-sizing:border-box;"></div>');
            }
            var o = $('#advert_'+id),
                close = '<a class="close" onclick="$(\'#advert_'+id+'\').fadeOut(200, function() { $(this).remove(); app.advShow=false; });" style="position: absolute; top: 20px; right: 20px;"><span class="icon icon-close"></span></a>';
          
            o.css({
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%',
                height: '100%',
                background: '#FFF',
                zIndex: 9999,
                overflowY: 'scroll'
            }).append(close+app.advertTpl(id,JSONResponse.data));
            
            app.advShow=true;

            $('.app').animate({
                scrollTop: 0
            }, 400);

        });
    },
    disallowAdvert: function(id) {
        var diag = $('<div></div>', {
            id: 'not_approve'
        }).css({
            zIndex: 10001
        }).html('<textarea id="reason" class="tinymce" style="height: 150px"></textarea>');
        diag.appendTo('.app');
        $('#not_approve').dialog({
            modal: true,
            title: 'Elutasítás indoklása',
            width: $(window).width(),
            height: $(window).height(),
            open: function() {
                setTimeout(function() {
                    Advert.initMCE();
                }, 500);
            },
            buttons: {
                'Küldés': function() {
                    tinymce.triggerSave();
                    if ($('#reason').val().length < 10) {
                        errorMessage('A megadott indoklás túl rövid! Kérjük, minimum 10 karaktert írjon!');
                    } else {
                        var _t = $(this);
                        app.ajax_request('ads_not_approved', {
                            id: id,
                            reason: $('#reason').val()
                        }, function(DATA) {
                            if (!DATA || DATA.success != 1) {
                                errorMessage('Hiba történt az elutasító üzenet küldése közben!');
                                return false;
                            }

                            infoMessage('Sikeres elutasítás! A hirdetőt e-mailben értesítette a rendszer!', 'Információ');
                            _t.dialog('close');
                        });
                    }
                },
                'Mégse': function() {
                    $(this).dialog('close');
                }
            },
            close : function() {
                if (tinymce.get('reason')) {
                    tinymce.get('reason').remove();
                }
                $('#not_approve').dialog('destroy');
                $('#not_approve').remove();
            }
        });
    },
    editAdvert: function(id) {
        app.ajax_request('show_advert', {id: id, a: Math.random() }, function(JSONResponse) {
            var data = JSONResponse.data, options = JSONResponse.options;
            $.get(url_base+'js/tpl/edit.tpl', function(html) {
                var box = $(html);
                if ($('#advert_'+id).length>0) {
                    $('#advert_'+id).fadeOut(200, function() {
                        $(this).remove(); 
                    });
                }
                box.appendTo('.app');
                box.rmodal('show');
                $('#advert_id').val(id);
                $('#uid').val(data.user_id);
                
                app.ajax_request('list_category', { a: Math.random() }, function(JSONResponse) {
                    if (!JSONResponse.data || !JSONResponse.data.length) {
                        return false;
                    }
                    $('#advert_category').find('option').not(':last').remove();
                    $.each(JSONResponse.data.reverse(), function(i, o) {
                        sel = data.category == o.id ? ' selected="selected"' : '';
                        $('#advert_category').prepend('<option value="'+o.id+'"'+sel+'>'+o.name+'</option>');
                    });
                });

                $('#advert_title').val(data.title);
                $('#advert_place').val(data.zip);
                $('#advert_city').val(data.city);
                $('#activate_end').html(data.activate_end);
                $('#advert_birth').val(data.birth);
                if (parseInt(data.active) == 1) {
                    $('#advert_active').prop('checked', 'checked');
                } else {
                    $('#advert_active').removeAttr('checked');
                }
                if (parseInt(data.need_pay) == 1) {
                    $('#advert_pay').prop('checked', 'checked');
                    $('#advert_pay_amount').val(data.price);
                } else {
                    $('#advert_pay').removeAttr('checked');
                    $('#advert_pay_amount').val('');
                }

                $('#expire').html(options.expire);

                var $images = '';
                $.each(JSON.parse(data.images), function(i, img) {
                    $images += 
                    '<div class="upload-wrap"> \
                        <img src="https://eladomobiltelefonok.hu'+img+'" alt=""> \
                        <input type="hidden" name="advert[images][]" class="ui-helper-hidden-accessible" value="'+img+'"> \
                        <a href="javascript:void(0);" onclick="removeImage($(this));"><span class="fa fa-trash"></span></a> \
                    </div>';
                });

                $('#image_upload').prepend($images);
                Advert.initEditor();

                setTimeout(function() {
                    tinymce.get('advert_desc').setContent(data.description);
                }, 500);


            }).fail(function(err) {
                console.log( JSON.stringify(err) );
            });
        });
    },
    getSettings: function() {
        app.ajax_request('notification_settings', { a: Math.random() }, function(JSONResponse) {
            if (JSONResponse.success != 1) {
                return;
            }
            var obj = $('#settings').find('.toggle');
            if (JSONResponse.enabled != 0) {
                if (!obj.hasClass('active')) {
                    $('#settings').find('.toggle').addClass('active');
                }
            }
        });
    },
    setupNoticitaion: function() {
        var obj = $('#settings').find('.toggle');
        if (obj.hasClass('active')) {
            var enabled = 1;
        } else {
            var enabled = 0;
        }

        app.ajax_request('set_notification', { enabled: enabled }, function(JSONResponse) {
            return;
        });
    },
    ajax_request: function(action, data, callback) {
        if (!$('#overlay').length) {
            $('<div id="overlay">... betöltés ...</div>').appendTo('body');
        }
        $('#overlay').css({
            width: $(window).width(),
            height: $(window).height(),
            background: 'rgba(0,0,0,0.5)',
            zIndex: 99999999,
            position: 'fixed',
            boxSizing: 'border-box',
            paddingTop: '120px',
            color: '#FFF',
            textAlign: 'center'
        });

        $.ajax({
            type: 'POST',
            crossDomain: true,
            dataType: 'json',
            url: 'https://eladomobiltelefonok.hu/api.php?action='+action,
            data: data,
            success: function(data) {
                $('#overlay').remove();
                callback(data);
            },
            error: function(xhr, status, error) {
                $('#overlay').remove();
                console.log(JSON.stringify(xhr));
                console.log(status);
                console.log(error);
           }
        });
    }
};

$(window).on('scroll touchmove', function() {
    var scrollHeight = $(document).height();
    var scrollPosition = $(window).height() + $(window).scrollTop();
    if (scrollPosition >= (scrollHeight-100) && app.loaded == true && app.advShow != true) {
        app.offset = app.offset + app.limit;
        app.search();
    }
});

$.fn.rmodal = function (display) {
    if (display === 'show') {
        $(this).show('slide', { direction: 'down' }, 1000).addClass('active');
        app.advShow = true;
    } else if (display === 'hide') {
        $(this).slideDown(function () {
            $(this).removeClass('active');
            app.advShow = false;
        });
    } else if (display === 'remove') {
        removeTinyMCE();
        $(this).slideDown(function () {
            $(this).remove();
            app.advShow = false;
        });
    } else {
        console.error('This type is not defined for opening a modal.');
    }
}