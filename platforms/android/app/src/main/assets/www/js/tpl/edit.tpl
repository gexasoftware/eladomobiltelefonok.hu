<div class="advertEdit modal">
	<header class="bar bar-nav">
		<a class="icon icon-close pull-right" href="javascript:" onclick="$('.advertEdit').rmodal('remove');"></a>
		<h1 class="title">Hirdetés szerkesztése</h1>
	</header>
	<div class="content">
		<form class="content-padded" method="post" action="" onsubmit="return false;">
			<div class="ui-state-highlight ui-widget ui-corner-all">
				<div class="ui-widget-content with-padding full-width">
					A hirdetés megjelenést követően <strong><span id="expire"></span> napig</strong> aktív / látható az oldalon! Ez követően frissíteni kell, hogy az újra megjelenjen. 
					<br>A hirdetés lejárati ideje: <strong id="activate_end"></strong>
				</div>
			</div>
			<input type="hidden" class="ui-helper-hidden-accessible" id="advert_id" value="">
			<input type="hidden" class="ui-helper-hidden-accessible" id="uid" value="">

			<div id="step1" class="steps">
				<h3 class="step-title">Alapadatok</h3>
				<div class="float-left full-width with-padding">
					<label for="advert_title">Hirdetés címe *</label>
					<input type="text" id="advert_title" required class="form-control" name="advert[title]" value="" placeholder="Adja meg a hirdetés címét! (pl. Cirmos cicák)">
				</div>
				<div class="float-left full-width with-padding">
					<div class="check-row">
						<input type="checkbox" id="advert_active" name="advert[active]" class="icheck" value="1">
						<label for="advert_active" class="margin-left float-left">
							Aktív / megjelenik az oldalon?
						</label>
					</div>
				</div>

				<div class="float-left full-width with-padding">
					<h3>Hirdetés leírása</h3>
					<div class="clear float-left full-width margin-top margin-bottom">
						<textarea name="advert[description]" id="advert_desc" required class="form-control tinymce"></textarea>
					</div>
				</div>
			</div>
			<div id="step2" class="steps">
				<h3 class="step-title">Kategória választása</h3>
				<div class="float-left full-width with-padding">
					<select name="advert[category]" id="advert_category" required data-placeholder="Válassza ki a megfelelő kategóriát!" class="form-control cz-select">
						<option></option>

						<option value="other">Egyéb kategória (A fentiek egyike sem!)</option>
					</select>
					<input type="text" id="other_category" name="advert[other_category]" placeholder="Itt adja meg a kategória nevét!" class="form-control margin-top hidden">
				</div>
			</div>

			<div id="step3" class="steps">
				<h3 class="step-title">Fellelhetőség</h3>
				<div class="float-left full-width with-padding">
					<label for="advert_place">Hol található?</label>
					<div class="row">
						<div class="col-lg-2 col-md-2 col-sm-3 col-xs-4">
							<input type="text" required id="advert_place" name="advert[zip]" value="" maxlength="4" placeholder="Ir.szám" class="form-control">
						</div>
						<div class="col-lg-10 col-md-10 col-sm-9 col-xs-8">
							<input type="text" required id="advert_city" name="advert[city]" value="" placeholder="Település" class="form-control">
						</div>
					</div>
				</div>
				<h3 class="step-title">Életkor</h3>
				<div class="float-left full-width with-padding">
					<label for="advert_birth">Mikor született?</label>
					<input type="text" id="advert_birth" name="advert[birth]" value="" placeholder="Születési idő" class="form-control date">
				</div>
			</div>
			<div id="step4" class="steps">
				<h3 class="step-title">Fotók</h3>
				<div class="float-left full-width with-padding">
					<label for="advert_images">Töltsön fel fotókat <small>(adminisztrátorként korlátlan fotót feltölthet!)</small></label>
					<div id="image_upload">
						<a href="javascript:;" id="uploader">
							<span>Kattintson ide a fájlok kiválasztásához!</span>
						</a>
					</div>
				</div>
			</div>
			<div id="step5" class="steps">
				<h3 class="step-title">Kiegészítő információk</h3>
				<div class="float-left full-width with-padding payable">
					<div class="check-row">
						<input type="checkbox" id="advert_pay" name="advert[need_pay]" class="icheck-special">
						<label for="advert_pay">
							Szükséges-e fizetni érte?
						</label>
					</div>
					<input type="text" id="advert_pay_amount" name="advert[price]" value="" placeholder="Összeg (Ft-ban)" class="form-control">
				</div>
				<div class="float-left full-width with-padding">
					<label>További lehetőségek</label>
					<div id="properties">

					</div>
				</div>
			</div>

			<button type="button" id="adv_submit" onclick="Advert.save();" class="btn btn-positive btn-block font24 submit float-right">
				Mentés &raquo;
			</button>
		</form>
	</div>
</div>