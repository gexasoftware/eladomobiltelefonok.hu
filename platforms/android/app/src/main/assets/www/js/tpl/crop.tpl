<div class="align-center float-left full-width">
	<div class="float-left full-width clear margin-bottom" style="display: none" id="crop_btns">
		<button class="btn btn-default" onclick="RotateLeft();"><span class="fa fa-undo"></span></button>
		<button class="btn btn-default" onclick="RotateRight();"><span class="fa fa-repeat"></span></button>
		<button class="btn btn-default" onclick="zoomIn();"><span class="fa fa-search-plus"></span></button>
		<button class="btn btn-default" onclick="zoomOut();"><span class="fa fa-search-minus"></span></button>
		<button class="btn btn-default" onclick="reset();"><span class="fa fa-search"></span></button>
	</div>
	<div class="float-left full-width clear cropper">
		<img src="" id="cropbox" />
	</div>
</div>
<br />
<form action="" id="cropperForm" method="post" onsubmit="return false;">
	<input type="hidden" id="fn" name="fn" value=""/>
	<input type="hidden" id="x" name="x" value="0" />
	<input type="hidden" id="y" name="y" value="0" />
	<input type="hidden" id="w" name="w" value="0" />
	<input type="hidden" id="h" name="h" value="1" />
	<input type="hidden" id="r" name="r" value="0" />
</form>