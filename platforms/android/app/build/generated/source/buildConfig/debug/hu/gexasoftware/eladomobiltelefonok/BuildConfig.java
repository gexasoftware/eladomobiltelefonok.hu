/**
 * Automatically generated file. DO NOT MODIFY
 */
package hu.gexasoftware.eladomobiltelefonok;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "hu.gexasoftware.eladomobiltelefonok";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 10000;
  public static final String VERSION_NAME = "1.0.0";
}
