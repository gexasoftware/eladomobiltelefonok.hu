
/* Messages */
function errorMessage(text) {

	if (!document.getElementById("errMsg")) {
		$('.app').append("<div id=\"errMsg\"></div>");
	}

	$("#errMsg").dialog({
		modal : true,
		title : 'Hiba történt!',
		autoOpen : true,
		width : $(window).width()-30,
		height : 'auto',
		resizable : false,
		open : function() {
			$(this).css("height", "auto").html(text);
		},
		buttons : {
			"OK" : function() {
				$(this).dialog("close");
			}
		},
		close : function() {
			$('#errMsg').remove();
		}
	});

	$(".ui-dialog").css("zIndex", 399900);
	$(".ui-widget-overlay").css("zIndex", 399000);
	return false;

}

function alertMessage(text, title) {

	if (!document.getElementById("alertMsg")) {
		$('.app').append("<div id=\"alertMsg\"></div>");
	}

	$("#alertMsg").dialog({
		modal : true,
		bgiframe : true,
		title : title,
		autoOpen : true,
		width: $(window).width()-30,
		open : function() {
			$(this).html(text);
		},
		buttons : {
			"OK" : function() {
				$(this).dialog("close");
			}
		},
		close : function() {
			$('#alertMsg').remove();
		}
	});

	$(".ui-dialog").css("zIndex", 399900);
	$(".ui-widget-overlay").css("zIndex", 399000);
	return false;

}

function infoMessage(text, title) {

	if (!document.getElementById("infoMsg")) {
		$('.app').append("<div id=\"infoMsg\"></div>");
	}

	$("#infoMsg").dialog({
		modal : true,
		bgiframe : true,
		title : title,
		autoOpen : true,
		width: $(window).width()-30,
		open : function() {
			$(this).html(text);
		},
		buttons : {
			"OK" : function() {
				$(this).dialog("close");
			}
		},
		close : function() {
			$('#infoMsg').remove();
		}
	});

	$(".ui-dialog").css("zIndex", 399900);
	$(".ui-widget-overlay").css("zIndex", 399000);
	return false;

}

function fnMessage(text, title, action, init) {

	if (!document.getElementById("fnMsg")) {
		$('.app').append("<div id=\"fnMsg\"></div>");
	}

	if (title == "") {
		title = 'Megerősítés';
	}

	$("#fnMsg").dialog({
		modal : true,
		title : title,
		width : $(window).width()-30,
		autoOpen : true,
		open : function() {
			$(this).html(text);
		},
		buttons : [{
			text : 'Rendben',
			click : function() {
				action();
				$(this).dialog("close");
			}
		}, {
			text : 'Mégse',
			click : function() {
				$(this).dialog("close");
			}
		}],
		close : function() {
			$('#fnMsg').remove();
		}
	});

	$(".ui-dialog").css("zIndex", 399900);
	$(".ui-widget-overlay").css("zIndex", 399000);

	if ( typeof init != 'undefined')
		init();

	return false;
}

