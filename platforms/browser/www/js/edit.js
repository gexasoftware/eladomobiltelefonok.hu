var delay = (function(){
	var timer = 0;
	return function(callback, ms){
		clearTimeout (timer);
		timer = setTimeout(callback, ms);
	};
})();

var removeTinyMCE = function() {
	tinymce.get('advert_desc').remove();
}

if (!Advert)
	var Advert = {};

Advert.initEditor = function() {
	Advert.initMCE();

	$('#advert_create .steps.incomplete:first').addClass('active');
	$('#advert_create .steps.incomplete').not(':first').addClass('hidden');
	
	$('#image_upload').sortable({
		items: '.upload-wrap'
	}).disableSelection();

	$('#advert_create').find('#adv_submit').on('click', function() {

		if (!$('.steps.incomplete').length && !$('.steps.hidden').length) {
			$('#advert_create').submit();
		}

		$('#advert_create .steps.active').each(function() {
			if ($(this).hasClass('incomplete')) {
				infoMessage('Előbb töltse ki az összes kötelező mezőt!');
				return false;
			} else if ( ($(this).next('.steps').hasClass('incomplete') || $(this).next('.steps').is('#step4') || $(this).next('.steps').is('#step5')) && $(this).next('.steps').hasClass('hidden') && !$(this).hasClass('incomplete')) {
				$(this).removeClass('active');
				var sbl = $(this).next('.steps');
				$(this).next('.steps').addClass('active').hide().removeClass('hidden').fadeIn(200);
				$('body,html').animate({
					scrollTop: sbl.offset().top
				},400);
			}
		});
	});

	$('#advert_city').autocomplete({
		source: 'https://orokbefogadlak.hu/api.php?action=get_city_list',
		minLength: 3,
		select: function( event, ui ) {
			$(this).val(ui.item.value).trigger('keyup');
		}
	}).on('keyup', function(e) {
		var _t = $(this);
		delay(function(){
			if (_t.val().length >= 3) {
				var v = _t.val(),
				type = _t.prop('id').split('_')[0];

				app.ajax_request('get_postcode', {
					city: v
				}, function(JSONResponse) {
					if (!JSONResponse || JSONResponse.success != 1) {
						$('#'+type+'_place').val('');
						$('#'+type+'_place').trigger('change');
						return;
					}
					$('#'+type+'_place').val(JSONResponse.postcode);
					$('#'+type+'_place').trigger('change');
				});
			}
		}, 500);
	});

	$('#advert_place').on('keyup', function(e) {
		var _t = $(this);
		delay(function(){
			if (_t.val().length >= 3) {
				var v = _t.val(),
				type = _t.prop('id').split('_')[0];

				app.ajax_request('get_city', {
					postcode: v
				}, function(JSONResponse) {
					if (!JSONResponse || JSONResponse.success != 1) {
						return;
					}
					$('#'+type+'_city').val(JSONResponse.city);
					$('#'+type+'_city').trigger('change');
				});
			}
		}, 500);
	});

	$('input.icheck-special').iCheck({
		checkboxClass: 'icheckbox_square-green float-left margin-right',
		increaseArea: '30%',
		labelHover: false,
		labelHoverClass: !is_mobile ? 'gb' : 'black'
	}).on('ifChecked', function() {
		$('#advert_pay_amount').removeAttr('disabled').focus();
	}).on('ifUnchecked', function() {
		$('#advert_pay_amount').prop('disabled', 'disabled');
	});

	$('input.icheck').not('.icheck-special').iCheck({
		checkboxClass: 'icheckbox_square-green float-left',
		increaseArea: '30%',
		labelHover: false,
		labelHoverClass: !is_mobile ? 'gb' : 'black'
	});

	$('#advert_category').on('change', function() {
		var _t = $(this).val();
		if (_t == 'other') {
			$(this).parents('.steps:first').addClass('incomplete');
			$('#other_category').removeClass('hidden').focus();
		} else {
			$(this).parents('.steps:first').removeClass('incomplete');
			$('#other_category').addClass('hidden');
		}

		Advert.loadProperties(_t);
	});

	if ($('#advert_id').val() > 0) {
		Advert.loadProperties($('#advert_category').val());
	}

	$(document).on('change', '.steps.active input.form-control', function() {
		var countRequiredVisible = $(this).parents('.steps.active:first').find('input.form-control, .cz-select').not('.hidden,.ui-helper-hidden-accessible').length;
		var succeedFields = 0;
		
		$(this).parents('.steps.active:first').find('input.form-control, .cz-select').each(function() {
			if ($(this).not('.hidden') && $(this).attr('required') == 'required') {
				if ($(this).attr('type') == 'text' && !$(this).hasClass('title') && $(this).val().length > 3) {
					succeedFields++;
				} else if ($(this).attr('type') == 'text' && $(this).hasClass('title') && $(this).val().length > 10) {
					succeedFields++;
				} else if ($(this).attr('type') != 'text' && $(this).attr('type') != 'checkbox' && $(this).attr('type') != 'file' && $(this).val() != '') {
					succeedFields++;
				} else {
					succeedFields++;
				}
			} else {
				succeedFields++;
			}
		});

		if (succeedFields >= countRequiredVisible || countRequiredVisible == 0) {
			$(this).parents('.steps:first').removeClass('incomplete');
		}
	});

};

Advert.initMCE = function() {
	if ('undefined' !== typeof(tinymce)) {
		tinymce.init({
			selector : ".tinymce",
			theme : "modern",
			language : 'hu_HU',
			width : '100%',
			skin: 'lightgray',
			height: 240,
			relative_urls : false,
			image_advtab : true,
			remove_script_host: false,
			plugins : ["advlist lists charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen link", "insertdatetime media nonbreaking save table contextmenu directionality emoticons textcolor paste"],
			toolbar1 : "bold italic alignleft aligncenter alignright alignjustify bullist numlist",
			toolbar2 : "undo redo fontsizeselect forecolor backcolor | link",
			menubar: false,
			fontsize_formats: "8px 10px 12px 14px 18px 24px 36px",
			force_br_newlines : true,
			force_p_newlines : true,
			forced_root_block: false,
			font_formats : "Open Sans=Open Sans,sans-serif;"+
			"Arial=arial,helvetica,sans-serif;"+
			"Arial Black=arial black,avant garde;"+
			"Book Antiqua=book antiqua,palatino;"+
			"Comic Sans MS=comic sans ms,sans-serif;"+
			"Courier New=courier new,courier;"+
			"Georgia=georgia,palatino;"+
			"Helvetica=helvetica;"+
			"Impact=impact,chicago;"+
			"Symbol=symbol;"+
			"Tahoma=tahoma,arial,helvetica,sans-serif;"+
			"Terminal=terminal,monaco;"+
			"Times New Roman=times new roman,times;"+
			"Trebuchet MS=trebuchet ms,geneva;"+
			"Verdana=verdana,geneva;"+
			"Webdings=webdings;"+
			"Wingdings=wingdings,zapf dingbats",
			setup : function(ed) { }
		});
	}
};

Advert.loadProperties = function(category) {
	app.ajax_request('property_editor', {
		id: $('#advert_id').val(),
		category: category
	}, function(JSONResponse) {
		if (!JSONResponse || JSONResponse.success != 1) {
			return;
		}

		$('#properties').html(JSONResponse.html);
		$('#properties').find('input.icheck').iCheck({
			checkboxClass: 'icheckbox_square-green float-left margin-right',
			radioClass: 'iradio_square-green float-left margin-right',
			increaseArea: '20%',
			labelHover: true,
			labelHoverClass: !is_mobile ? 'gb' : 'black'
		});

	});
}

Advert.save = function() {
	var props = [];
	$('#properties').find('input').each(function(i, o) {
		if ($(this).is(':checkbox')) {
			if ($(this).is(':checked')) {
				props.push({ type: 'check', id: $(this).data('propid'), value: 1 });
			}
		} else {
			props.push({ type: 'text', id: $(this).data('propid'), value: $(this).val() });
		}
	});
	
	var images = [];
	$('#image_upload').find('input[type=hidden]').each(function(i, o) {
		images.push($(this).val());
	});

	tinymce.triggerSave();

	app.ajax_request('save_advert', { 
		data: {
			id: $('#advert_id').val(),
			title: $('#advert_title').val(),
			active: $('#advert_active').is(':checked') ? 1 : 0,
			description: $('#advert_desc').val(),
			category: $('#advert_category').val(),
			other_category: ($('#advert_category').val()=='other'?$('#other_category').val():''),
			city: $('#advert_city').val(),
			zip: $('#advert_place').val(),
			birth: $('#advert_birth').val(),
			need_pay: $('#advert_pay').is(':checked') ? 1 : 0,
			price: $('#advert_pay_amount').val(),
			images: images,
			props: props
		}
	}, function(JSONResponse) {
		if (!JSONResponse || JSONResponse.success != 1) {
			errorMessage('Hiba történt az adatok mentése közben!');
			return;
		}
		infoMessage(JSONResponse.message, 'Sikeres mentés');
	});
}


$(document).on('click', '#uploader', function(event) {
	navigator.camera.getPicture(function(url) {
		upload(url);
	}, 
	function(err) {
		console.log('error = '+err);
	},
	{ 
		quality: 90,
		sourceType: Camera.PictureSourceType.PHOTOLIBRARY, 
		allowEdit: false,
		destinationType: navigator.camera.DestinationType.FILE_URL
	});
});

var cropper;
function upload(URL) {
	var fileURL = URL;

	var success = function (r) {
		var resp = JSON.parse(r.response), url = resp.url;
		if (resp.success != 1) {
			errorMessage('Túl nagy képet próbált feltölteni! A maximális méret 2MB!');
			return;
		} else {
			$.get(url_base+'js/tpl/crop.tpl', function(html) {
				var saved = false;
				if (!$('#cropper').length) {
					$('#cropper').remove();
				}
				$('<div id="cropper" style="z-index: 999999"></div>').appendTo('.app');

				$('#cropper').html(html).dialog({
					title: 'Kérjük vágja meg a képet megfelelő méretre!',
					width: $(window).width(),
					height: $(window).height() * 0.95,
					resizable: false,
					draggable: false,
					modal: true,
					buttons: [
					{
						text: 'Mentés',
						click: function() {
							var dObj = $(this);
							var x = $('#cropperForm').find('#x').val();
							var y = $('#cropperForm').find('#y').val();
							var w = $('#cropperForm').find('#w').val();
							var h = $('#cropperForm').find('#h').val();
							var r = $('#cropperForm').find('#r').val();
							app.ajax_request('image_crop', {
								x: x,
								y: y,
								w: w,
								h: h,
								r: r,
								url: url
							}, function(DATA) {
								if (!DATA) {
									return false;
								}

								if (DATA.success != 0) {
									var tmpUrl = 'https://orokbefogadlak.hu'+(DATA.url).replace('//', '/');
									DATA.url = (DATA.url).replace('//', '/');

									var imgTpl = $('<div class="upload-wrap"></div>');
									imgTpl.append('<img src="'+tmpUrl+'?a='+Math.random()+'">');
									//alert(tmpUrl);
									imgTpl.append('<input type="hidden" name="advert[images][]" class="ui-helper-hidden-accessible" value="'+DATA.url+'">');
									imgTpl.append('<a href="javascript:void(0);" onclick="removeImage($(this));"><span class="fa fa-trash"></span></a>');
									$('#image_upload').prepend(imgTpl);
									$('#image_upload').sortable({
										items: '.upload-wrap'
									}).disableSelection();
									saved = true;

								} else {
									errorMessage(DATA.message);
									saved = false;
								}

								dObj.dialog('close');
							});
						}
					},
					{
						text: 'Mégse',
						click: function() {
							$(this).dialog('close');
						}
					}
					],
					open: function() {

						$('#cropbox').prop('src', 'https://orokbefogadlak.hu'+url);
						var w,h;
						$('#cropbox').on('load', function() {
							w = document.getElementById('cropbox').naturalWidth;
							h = document.getElementById('cropbox').naturalHeight;
							$('#w').val(w);
							$('#h').val(h);

							var image = document.getElementById('cropbox');
							cropper = new Cropper(image, {
								aspectRatio: 1,
								background: true,
								movable: false,
								rotatable: true,
								scalable: false,
								zoomOnWheel: false,
								autoCropArea: 1,
								viewMode: 1,
								minCropBoxWidth: 200,
								minCropBoxHeight: 200,
								crop: function(e) {
									$('#x').val(e.detail.x);
									$('#y').val(e.detail.y);
									$('#w').val(e.detail.width);
									$('#h').val(e.detail.height);
									$('#r').val(e.detail.rotate);
								}
							});
						});
						$('#fn').val(url);

						var dialog = $(this).closest('.ui-dialog').css({top: 0, background: 'white' });
						var originalButtons = $('.ui-dialog-buttonpane', dialog).addClass('font18');
						var clonedButtons = originalButtons.clone().addClass('clone font18 full-width');
						$('.ui-dialog-titlebar', dialog).after(clonedButtons);
						clonedButtons.find('.ui-dialog-buttonset').addClass('full-width').css({'text-align': 'center'});
						$('.ui-dialog-titlebar', dialog).hide();
						originalButtons.hide();
						$('.ui-button', clonedButtons).click(function() {
							var button = $(this);
							var buttonIndex = $('button', clonedButtons).index(button);
							$('button:eq(' + (buttonIndex-5) + ')', originalButtons).click();
						});
						dialog.css({height: $(window).innerHeight() });
						//$('body').css({overflowX: 'hidden', overflowY: 'scroll'});
						$('html,body').animate({scrollTop: 0}, 500);
						$('body').css({ overflow: 'hidden' });

						$('#cropper').css({background: 'white', height: $(window).innerHeight()-10 });
						$('#cropper .cropper').css({height: $(window).innerHeight()-20 });
						var btns = $('#crop_btns').html();
						clonedButtons.find('.ui-button:first').before(btns);
						$(window).on('resize', function() {
							if ($('#cropper').length == 1) {
								dialog.css({width: $(window).innerWidth(), height: $(window).innerHeight(), top: 0 });
								$('#cropper').css({
									width: $(window).innerWidth()-10,
									height: $(window).innerHeight()-10
								});
							}
						});
					},
					close: function() {
						$('body').css({overflow: 'auto'});
						if (!saved) {
							app.ajax_request('remove_image', {
								url: url
							});
						}
						$(this).dialog('destroy');
						$("#cropper").remove();
					}
				});
			});
		}
	}

	var fail = function (error) {
		console.log(JSON.stringify(error));
		alert("An error has occurred: Code = " + error.code);
	}

	var options = new FileUploadOptions();
	options.fileKey = "fileUpload";
	options.fileName = fileURL.substr(fileURL.lastIndexOf('/') + 1);
	options.mimeType = "image/jpeg";
	options.httpMethod="POST";
	options.chunkedMode=false;

	var params = {};
	params.uid = $('#uid').val();
	options.params = params;

	var ft = new FileTransfer(),
	SERVER = 'https://orokbefogadlak.hu/api.php?action=upload_advert_image';

	ft.upload(fileURL, encodeURI(SERVER), success, fail, options);
};

function RotateLeft() { cropper.rotate(-90); }
function RotateRight() { cropper.rotate(90); }
function zoomIn() { cropper.zoom(1); }
function zoomOut() { cropper.zoom(-1); }
function reset() { cropper.reset(); }

function removeImage(data) {
	var parent = data.parent(),
	img = parent.find('input[type=hidden]').val(),
	id = $('#advert_id').length > 0 ? $('#advert_id').val() : 0;

	var editText = id == 0 ? '' : '<br>Amennyiben törli a fotót, az azonnal törlődni fog a hirdetésből!<br><strong>Biztos folytatja?</strong>';

	fnMessage('Biztos törölni szeretné a feltöltött fotót?'+editText, 'Megerősítés szükséges', function() {
		app.ajax_request('remove_image', {
			url: img,
			id: id
		}, function(DATA) {
			if (!DATA || DATA.success != 1) {
				errorMessage('Hiba történt a kép törlése közben!');
				return;
			}

			parent.fadeOut(200, function() { $(this).remove(); });
		});
	});
}

function number_format (number, decimals, dec_point, thousands_sep) {
	number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
	var n = !isFinite(+number) ? 0 : +number,
	prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
	sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
	dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
	s = '',
	toFixedFix = function (n, prec) {
		var k = Math.pow(10, prec);
		return '' + Math.round(n * k) / k;
	};
  // Fix for IE parseFloat(0.55).toFixed(0) = 0;
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
  if (s[0].length > 3) {
  	s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
  }
  if ((s[1] || '').length < prec) {
  	s[1] = s[1] || '';
  	s[1] += new Array(prec - s[1].length + 1).join('0');
  }
  return s.join(dec);
}

window.onerror = function(message, url, lineNumber) {
	console.log("Error: "+message+" in "+url+" at line "+lineNumber);
}